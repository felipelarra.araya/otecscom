<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/






Route::get('/capacitacion', 'CapacitacionFrontController@index');

Route::get('/', 'HomePrincipalController@index');


Route::get('/detalleCurso/{id}/edit', 'CapacitacionFrontController@edit');

Route::get('/EvaluacionesPsicotecnicas', 'EvaluacionesFrontController@index');
Route::get('/detalleExamen/{id}/edit', 'EvaluacionesFrontController@edit');



Route::get('/acreditacion', function () {
    return view('layouts.acreditacion');
});

Route::get('/contacto', function () {
    return view('layouts.contacto');
});

Route::get('/escuelaConductores', function () {
    return view('layouts.escuelaConductores');
});



Route::get('/sobreNosotros', 'SobreNosotrosFrontController@index');
Route::get('/acreditacion', 'AcreditacionFrontController@index');
Route::get('/escuelaConductores', 'EscuelaConductoresFrontController@index');


/*----------------------------Rutas Admin-----------------------------------*/ 
Route::group(['prefix'=>'admin','as'=>'admin' ], function(){
    Route::get('/',function () { return view('layouts.admin.main');});
    Route::get('/evaluacionesPsicotecnicas', 'EvaluacionesController@index');
    Route::get('/examenes', 'EvaluacionesController@verExamenes');
    Route::get('/examenes/{id}/delete', 'EvaluacionesController@getExamenDelete');
    Route::get('/examenes/{id}/edit', 'EvaluacionesController@getExamenesEdit');

    Route::get('/sobreNosotros', 'SobreNosotrosController@index');
    Route::get('/sobreNosotros/{id}/edit', 'SobreNosotrosController@getSobreNosotrosEdit');
    Route::post('/sobreNosotros/modificarSobreNosotros', 'SobreNosotrosController@editarSobreNosotros');
    
    Route::get('/acreditacion', 'AcreditacionController@index');
    Route::get('/acreditacion/{id}/edit', 'AcreditacionController@getAcreditacionEdit');
    Route::post('/acreditacion/modificarAcreditacion', 'AcreditacionController@editarAcreditacion');

    Route::get('/escuelaConductores', 'EscuelaConductoresController@index');
    Route::get('/escuelaConductores/{id}/edit', 'EscuelaConductoresController@getEscuelaEdit');
    Route::post('/escuelaConductores/modificarEscuelaConductores', 'EscuelaConductoresController@editarEscuelaConductores');

    Route::get('/evaluacionesPsicotecnicas/add', 'EvaluacionesController@getExamenesAdd');
    Route::get('/evaluacionesPsicotecnicas/{id}/edit', 'EvaluacionesController@getExamenesEdit');
    Route::get('/evaluacionesPsicotecnicas/{id}/editEvaluacion', 'EvaluacionesController@getEvaluacionesEdit');
    Route::post('/evaluacionesPsicotecnicas/edit', 'EvaluacionesController@editarExamen');
    Route::post('/evaluacionesPsicotecnicas/modificarEvaluacion', 'EvaluacionesController@editarSeccionEvaluacion');
    Route::resource('evaluacionesPsicotecnicas','EvaluacionesController');

    /*------------------Capacitacion-----------------------*/

    Route::get('/capacitacion', 'CapacitacionController@index');
    Route::get('/capacitacion/{id}edit', 'CapacitacionController@edit');
    Route::post('/capacitacion/edit', 'CapacitacionController@editarCapacitacion');
    Route::resource('capacitacion','CapacitacionController');

    Route::get('/cursos', 'CursosController@index');
   //Route::get('/categoriaCursos', 'CategoriasCursosController@getHome');
    Route::get('/categoriaCursos/add', 'CategoriasCursosController@getCategoriasAdd');
    Route::get('/categoriaCursos/{id}edit', 'CategoriasCursosController@edit');
    Route::post('/categoriaCursos/edit', 'CategoriasCursosController@editarCategoria');
    Route::get('/categoriaCursos/{id}/delete', 'CategoriasCursosController@getCategoriaDelete');
    Route::resource('categoriaCursos','CategoriasCursosController');

    /*----------------Curso--------------------------*/

    Route::get('/cursos/add', 'CursosController@getCursoAdd');
    Route::post('/cursos/edit', 'CursosController@editarCurso');
    Route::get('/cursos/{id}edit', 'CursosController@edit');
    Route::get('/cursos/{id}/delete', 'CursosController@getCursoDelete');
    Route::resource('cursos','CursosController');

    /*-------------------------Servicios------------------------------- */

    Route::get('/servicios', 'ServiciosController@index');
    Route::get('/servicios/add', 'ServiciosController@getServicioAdd');
    Route::post('/servicios/edit', 'ServiciosController@editarServicio');
    Route::get('/servicios/{id}/delete', 'ServiciosController@getServicioDelete');
    Route::resource('servicios','ServiciosController');

    /*-------------------------Servicios------------------------------- */

    Route::get('/alumnos', 'AlumnosController@index');
    Route::get('/alumnos/add', 'AlumnosController@getAlumnoAdd');
    Route::post('/alumnos/edit', 'AlumnosController@editarAlumno');
    Route::get('/alumnos/{id}/delete', 'AlumnosController@getAlumnoDelete');
    Route::resource('alumnos','AlumnosController');


    
    /*----------------------Slider-------------*/

    Route::get('/sliders', 'SliderController@index');
    Route::get('/sliders/add', 'SliderController@getSliderAdd');
    Route::get('/sliders/{id}edit', 'SliderController@edit');
    Route::post('/sliders/edit', 'SliderController@editarSlider');
    Route::get('/sliders/{id}/delete', 'SliderController@getSliderDelete');
    Route::resource('sliders','SliderController');
    

    
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/mail', 'MailController@store');

