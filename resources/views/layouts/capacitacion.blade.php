@extends('layouts.main')
@section('title','Capacitación')

@section('contenido')
<div class="inner-page">
@foreach($capacitacion as $c)
    <div class="slider-item" style="background-image: url('{{ asset('images/capacitacion/'.$c->imagen) }}');">
        
        <div class="container">
          <div class="row slider-text align-items-center justify-content-center">
            <div class="col-md-8  col-sm-12 element-animate pt-5">
              <h1 class="pt-5"><span></span>Capacitación</h1>
            </div>
          </div>
        </div>

      </div>

    </div>

    <div class="card-img-overlay formSlider">
  
    <div class="container">
        <div class="row justify-content-end align-items-center">
            
            <div class="col-sm-4">
                <div class="card shadow-lg">
                  
                    <div class="card-body">
                      <div class="container">
                        <div class="row">
                          @if($message = Session::get('Listo'))
                            <div class="col-12 alert alert-success alert-dismissable fade show" id="mensajeExito" role="alert"> 
         
                              <span>{{ $message }}</span>  
                            </div>
                          @endif

                          @if($message = Session::get('ErrorInsert'))

                            <div class="col-12 alert alert-danger alert-dismissable fade show" role="alert">
            
                              <ul>
                                @foreach($errors->all() as $error)
                                  <li>{{ $error }}</li>

                                @endforeach
                              </ul>  
                            </div>

                          @endif
    
                        </div>
                      </div>
                    <form action="/mail" method="post">
                      @csrf
                            <div class="form-group">
                                
                                <div class="form-group">
                                    <input type="text" class="form-control" name="nombre" placeholder="Nombre" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="email"  placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="telefono"  placeholder="Teléfono" required>
                                </div>
                                <div class=" form-group">
                                
                                <textarea name="mensaje" placeholder="Mensaje" id="message" class="form-control" cols="30" rows="3" required></textarea>
                              </div>
                                <input type="submit" value="Postular" class="btn btn-success btn-block">

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <section class="section border-t pb-0">
      <!--<div class="container">
        <div class="row  mb-5 element-animate">
          <div class="col-md-6 col-sm-12">
            <h2 class="heading">{{ $c->nombre }}</h2>
           
            <p class="mb-2">{!! $c->descripcion !!}</p>
            
          </div>
         

          <div class="col-md-6 col-sm-12">
          <img src="{{ asset('images/capacitacion/'.$c->imagen) }}" alt="Capacitación cursos La Serena minería" class="img-fluid">
            </div>
        </div>
      </div>-->
      @endforeach
      <!--<hr style="width:75%;">-->
      
      <div class="container">
      <h2 class="heading col-12 text-center">Cursos</h2>
      <div class="wrap">
		<div class="store-wrapper">
			<div class="category_list">
				
  <div class="panel panel-default">
  <!-- List group -->
  <ul class="list-group">
  @foreach($categorias as $categoria)
    <li class="list-group-item text-center"><a href="#" class="category_item" category="{{$categoria->nombre}}">{{$categoria->nombre}}</a></li>
  @endforeach
  <li class="list-group-item text-center"><a href="#" class="category_item" category="all">Todo</a></li>
  </ul>
</div>
				
			</div>
			<section class="products-list mt-2">
      <div class="container page-top">
  <div class="row">
@foreach($cursosCategoria as $curso)
    <div class="itemCurso col-lg-3 col-md-4 col-xs-6  mt-5" category="{{ $curso->categoriaNombre }}">
            <a href="{{ url('detalleCurso/'.$curso->id.'/edit')}}">
            <img  src="{{ asset('images/cursos/'.$curso->imagen) }}" alt="Cursos Minería La Serena Coquimbo" class="zoom img-fluid"> 
            {{ $curso->nombre }}   
          </a>
    </div>
    @endforeach
    
    
   
   
</div>




</div>

        
    </section>
    <!-- END section -->

    

</div>  



  
 
@endsection

@section('scripts')


<script>

$(document).ready(function(){

  setTimeout(function() {
            $("#mensajeExito").fadeOut(1500);
        },3000);
// AGREGANDO CLASE ACTIVE AL PRIMER ENLACE ====================
$('.category_list .category_item[category="all"]').addClass('ct_item-active');

// FILTRANDO PRODUCTOS  ============================================

$('.category_item').click(function(e){
  var catProduct = $(this).attr('category');
  e.preventDefault();
  

  // AGREGANDO CLASE ACTIVE AL ENLACE SELECCIONADO
  $('.category_item').removeClass('ct_item-active');
  $(this).addClass('ct_item-active');

  // OCULTANDO PRODUCTOS =========================
  $('.itemCurso').css('transform', 'scale(0)');
  function hideProduct(){
    $('.itemCurso').hide();
  } setTimeout(hideProduct,400);

  // MOSTRANDO PRODUCTOS =========================
  function showProduct(){
    $('.itemCurso[category="'+catProduct+'"]').show();
    $('.itemCurso[category="'+catProduct+'"]').css('transform', 'scale(1)');
  } setTimeout(showProduct,400);
});

// MOSTRANDO TODOS LOS PRODUCTOS =======================

$('.category_item[category="all"]').click(function(){
  function showAll(){
    $('.itemCurso').show();
    $('.itemCurso').css('transform', 'scale(1)');
  } setTimeout(showAll,400);
});
});
</script>
@endsection