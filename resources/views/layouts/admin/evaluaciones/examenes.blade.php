@extends('layouts.admin.main')
@section('contenido')  


<!---------------------Contenido para la entidad Examen------------------->
<div class="d-sm-flex align-items-center justify-content-between mb-4 mt-5">
            <h1 class="h3 mb-0 text-gray-800">Exámenes</h1>
            <a href="{{ url('/admin/evaluacionesPsicotecnicas/add')}}" class=" d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fa fa-plus fa-sm text-white-50"></i> Agregar Examen</a>
          </div>
          <div class="row">
        @if($message = Session::get('Listo'))
          <div class="col-12 alert alert-success alert-dismissable fade show" id="mensajeExito" role="alert"> 
          <h5>Mensaje:</h5>
        <span>{{ $message }}</span>  
        </div>
        @endif
</div>  
         
    <table class="table col-12 table-bordered table-hover">
      <thead>
      
      @if(count($examenes)>0)
        <tr>
          
          <th scope="col">Nombre</th>
          <th scope="col">Imagen</th>
          <th scope="col">Acciones</th>
          
        </tr>
      </thead>
      <tbody>
      @foreach($examenes as $examen)
        <tr>
          <td>{{ $examen->nombre }}</td>
          <td> <a href = "{{ asset('images/examenes/'.$examen->imagen) }}" data-fancybox="gallery">
          <img src="{{ asset('images/examenes/thumbs/'.$examen->imagen) }}" class="img-fluid"> </td>
        </a>
          <td>
          <button class="btn btn-round btnEditar"
          data-id="{{ $examen->id }}"
          data-name="{{ $examen->nombre }}"
          data-descripcion="{{ $examen->descripcion }}"> 
          <a href="{{ url('/admin/examenes/'.$examen->id.'/edit')}}"><i class="fa fa-edit"></i></a> 
          </button>
          <button class="btn btn-round btnEliminar" data-toggle="modal" data-target="#modalEliminar{{$examen->id}}"><i class="fa fa-trash"></i> 
        </button>
          </td>
        </tr>
         <!-- Modal Eliminar -->
<div class="modal fade" id="modalEliminar{{$examen->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar Examen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     

        <div class="modal-body">
        <h5>¿Desea eliminar Examen?</h5>
        
        </div>
           
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
         
          <a type="button" class="btn btn-danger" href="{{ url('/admin/examenes/'.$examen->id.'/delete')}}">Si</a> 
        </div>
 
    </div>
  </div>
</div>
        @endforeach
      </tbody>
    </table>
    {{$examenes->links()}}
        
@else
{{"No existen Registros"}}
<br>
<br>
<br>
@endif

</div>
@endsection

@section('scripts')
<script>
$(document).ready(function() {
    
   
    setTimeout(function() {
        $("#mensajeExito").fadeOut(1500);
    },3000);

   
});
</script>

@endsection
