@extends('layouts.admin.main')
@section('contenido')  

<h2>Sección Evaluaciones Psicotécnicas</h2>
<div class="row">
        @if($message = Session::get('Listo'))
          <div class="col-12 alert alert-success alert-dismissable fade show" id="mensajeExito" role="alert"> 
          <h5>Mensaje:</h5>
        <span>{{ $message }}</span>  
        </div>
        @endif
</div>  
      <table class="table col-12">
      <thead>
      
        <tr>
          <th scope="col">Id</th>
          <th scope="col">Nombre</th>
          <th scope="col">Imagen</th>
          <th scope="col">Modificar</th>
        </tr>
      </thead>
      <tbody>
      @foreach($evaluacion as $evaluaxion)
        <tr>
          <td>{{ $evaluaxion->id }}</td>
          <td>{{ $evaluaxion->nombre }}</td>
          <td> <a href = "{{ asset('images/evaluaciones/'.$evaluaxion->imagen) }}" data-fancybox="gallery">
          <img src="{{ asset('images/evaluaciones/thumbs/'.$evaluaxion->imagen) }}" class="img-fluid"> </td>
        </a>
          
          <td>
          <button class="btn btn-round btnEditar">
          <a href="{{ url('/admin/evaluacionesPsicotecnicas/'.$evaluaxion->id.'/editEvaluacion')}}"><i class="fa fa-edit"></i></a> 
          
          </button>
         
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
<br>
<br>

<hr>

</div>
@endsection


@section('scripts')
<script>
$(document).ready(function() {
    setTimeout(function() {
        $("#mensajeExito").fadeOut(1500);
    },3000);
});
</script>

@endsection
