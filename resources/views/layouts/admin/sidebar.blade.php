<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/admin/sliders">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Panel de Administración</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      
      <li class="nav-item">
        <a class="nav-link" href="/admin/sliders">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Slider</span></a>
      </li>


      <li class="nav-item">
        <a class="nav-link" href="/admin/servicios">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Servicios</span></a>
      </li>

       <!-- Nav Item - Charts -->
       <li class="nav-item">
        <a class="nav-link" href="/admin/sobreNosotros">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Sobre Nosotros</span></a>
      </li>


      
      <li class="nav-item">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span>Evaluaciones</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="/admin/evaluacionesPsicotecnicas">Sección Evaluaciones Psicotécnicas</a>
          <a class="dropdown-item" href="/admin/examenes">Exámenes</a>
        </div>
      </li>

      
      <li class="nav-item">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span>Capacitación</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="/admin/cursos">Cursos</a>
          <a class="dropdown-item" href="/admin/categoriaCursos">Categorías</a>
          <a class="dropdown-item" href="/admin/capacitacion">Sección Capacitación</a>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="/admin/acreditacion">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Acreditación</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="/admin/escuelaConductores">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Escuela de Conductores</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/admin/alumnos">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Testimonios</span></a>
      </li>

    </ul>