@extends('layouts.admin.main')
@section('contenido')  



<!---------------------Contenido para la entidad Curso------------------->
<div class="d-sm-flex align-items-center justify-content-between mb-4 mt-5">
            <h1 class="h3 mb-0 text-gray-800">Categorías</h1>
           
          </div>
          <div class="row">
        @if($message = Session::get('Listo'))
          <div class="col-12 alert alert-success alert-dismissable fade show" id="mensajeExito" role="alert"> 
          <h5>Mensaje:</h5>
        <span>{{ $message }}</span>  
        </div>
        @endif
</div>  
         
            @if($message = Session::get('ErrorInsert'))

            <div class="col-12 alert alert-danger alert-dismissable fade show" role="alert">
              <h5>Errores:</h5>
            <ul>
              @foreach($errors->all() as $error)
            <li>{{ $error }}</li>

            @endforeach
            </ul>  
           
</div>
            @endif
        <div class="row">
        <div class="col-md-6">
        <form action="/admin/categoriaCursos" enctype="multipart/form-data" method="post">
      @csrf  
        <div class="form-group">
        <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{old('nombre')}}">
          </div>
          <div class="form-group">
          <input type="file"  class="form-control" name="imagen" id="imagen" size="30" >
          <button type="submit" class="mt-3 btn btn-primary">Guardar</button>
          </div>
    </div>
    
    </form>

    <div class="col-md-6">
      
     
      <table class="table col-12 table-bordered table-hover">
      <thead>
      
      @if(count($categorias)>0)
        <tr>
          
          <th scope="col">Nombre</th>
          <th scope="col">Imagen</th>
          <th scope="col">Acciones</th>
          
          
        </tr>
      </thead>
      <tbody>
      @foreach($categorias as $categoria)
        <tr>
          <td>{{ $categoria->nombre }}</td>
          <td> <a href = "{{ asset('images/categoriasCursos/'.$categoria->imagen) }}" data-fancybox="gallery">
          <img src="{{ asset('images/categoriasCursos/thumbs/'.$categoria->imagen) }}" class="img-fluid"> </td>
        </a>
          <td>
          <button class="btn btn-round btnEditar"> 
          <a href="{{ url('/admin/categoriaCursos/'.$categoria->id.'/edit')}}"><i class="fa fa-edit"></i></a> 
          </button>
          
          <button class="btn btn-round btnEliminar"  data-toggle="modal" data-target="#modalEliminar{{$categoria->id}}"><i class="fa fa-trash"></i> 
          </button>
          </td>

        </tr>
        
<!-- Modal Eliminar -->
<div class="modal fade" id="modalEliminar{{$categoria->id}}"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar Categoría</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     

        <div class="modal-body">
        <h5>¿Desea eliminar la categoría?</h5>
        
        </div>
           
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
          <a type="button" class="btn btn-danger" href="{{ url('/admin/categoriaCursos/'.$categoria->id.'/delete')}}">Si</a> 
        </div>
 
    </div>
  </div>
</div>

        @endforeach
      </tbody>
    </table>
    {{$categorias->links()}}
        
@else
{{"No existen Registros"}}
<br>
<br>
<br>
@endif
            
    </div>
        </div>
    
</div>

@endsection


@section('scripts')
<script>
$(document).ready(function() {
      
    setTimeout(function() {
        $("#mensajeExito").fadeOut(1500);
    },3000);

   
});
</script>

@endsection