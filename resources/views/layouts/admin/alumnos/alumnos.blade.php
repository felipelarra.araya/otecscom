@extends('layouts.admin.main')
@section('contenido')  



<!---------------------Contenido para la entidad Curso------------------->
<div class="d-sm-flex align-items-center justify-content-between mb-4 mt-5">
            <h1 class="h3 mb-0 text-gray-800">Testimonios alumnos</h1>
            <a href="{{ url('/admin/alumnos/add')}}" class=" d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fa fa-plus fa-sm text-white-50"></i> Agregar Testimonio</a>
          </div>
          <div class="row">
        @if($message = Session::get('Listo'))
          <div class="col-12 alert alert-success alert-dismissable fade show" id="mensajeExito" role="alert"> 
          <h5>Mensaje:</h5>
        <span>{{ $message }}</span>  
        </div>
        @endif
</div>  
         
    <table class="table col-12 table-bordered table-hover">
      <thead>
      
      @if(count($alumnos)>0)
        <tr>
          
          <th scope="col">Nombre</th>
          <th scope="col">Imagen</th>
          <th scope="col">Acciones</th>
          
        </tr>
      </thead>
      <tbody>
      @foreach($alumnos as $alumno)
        <tr>
          <td>{{ $alumno->nombre }}</td>
          <td> <a href = "{{ asset('images/alumnos/'.$alumno->imagen) }}" data-fancybox="gallery">
          <img src="{{ asset('images/alumnos/thumbs/'.$alumno->imagen) }}" width=100 > </td>
        </a>
          <td>
          <button class="btn btn-round btnEditar"> 
          <a href="{{ url('/admin/alumnos/'.$alumno->id.'/edit')}}"><i class="fa fa-edit"></i></a> 
          </button>
          <button class="btn btn-round btnEliminar" data-toggle="modal" data-target="#modalEliminar{{$alumno->id}}"><i class="fa fa-trash"></i> 
          </button>
          </td>
        </tr>
         <!-- Modal Eliminar -->
<div class="modal fade" id="modalEliminar{{$alumno->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar Testimonio</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     

        <div class="modal-body">
        <h5>¿Desea eliminar el testimonio?</h5>
        
        </div>
           
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
         
          <a type="button" class="btn btn-danger" href="{{ url('/admin/alumnos/'.$alumno->id.'/delete')}}">Si</a> 
        </div>
 
    </div>
  </div>
</div>
        @endforeach
      </tbody>
    </table>
    {{$alumnos->links()}}
        
@else
{{"No existen Registros"}}
<br>
<br>
<br>
@endif

          </div>


    </div>
  </div>
</div>


<!-- Modal Eliminar -->
<div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar Usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     

        <div class="modal-body">
        <h5>¿Desea eliminar usuario?</h5>
        
        </div>
           
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="button" class="btn btn-danger btnModalEliminar">Eliminar</button>
        </div>
 
    </div>
  </div>
</div>

@endsection



@section('scripts')
<script>
$(document).ready(function() {
    
   
    setTimeout(function() {
        $("#mensajeExito").fadeOut(1500);
    },3000);

   
});
</script>

@endsection