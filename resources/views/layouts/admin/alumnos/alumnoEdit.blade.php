@extends('layouts.admin.main')
@section('contenido')  

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Testimonio Alumno</h1>
            
          </div>
          <div class="row">
        @if($message = Session::get('Listo'))
          <div class="col-12 alert alert-success alert-dismissable fade show" id="mensajeExito" role="alert"> 
          <h5>Mensaje:</h5>
        <span>{{ $message }}</span>  
        </div>
        @endif
</div>  




<form action="/admin/alumnos/edit" enctype="multipart/form-data" method="post">
      @csrf

        
            @if($message = Session::get('ErrorInsert'))

            <div class="col-12 alert alert-danger alert-dismissable fade show" role="alert">
              <h5>Errores:</h5>
            <ul>
              @foreach($errors->all() as $error)
            <li>{{ $error }}</li>

            @endforeach
            </ul>  
            </div>

            @endif

        <div class="row">
        <div class="col-md-6">
        <div class="form-group">
        <input type="hidden" name="id" id="idEdit" value="{{$alumno->id}}">
            <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{$alumno->nombre}}">
          </div>
          <div class="form-group">
          <textarea class="form-control"  placeholder="Descripción" id="descripcionAlumno" name="descripcionAlumno" rows="5">{{$alumno->descripcion}}</textarea>
          </div>
          <div class="form-group">
          <input type="file"  class="form-control" name="imagen" id="imagen" size="30" >
          <button type="submit" class="mt-3 btn btn-primary">Guardar</button>
          </div>
    </div>
    
    </form>

    <div class="col-md-4">
        
            <div class="header">
            <h1 class="h3 mb-0 text-gray-800">Icono</h1>
            <div class="inside">
            <img src="{{ asset('images/alumnos/'.$alumno->imagen) }}" class="img-fluid">
            </div>
          

        </div>
    </div>
        </div>
    
</div>

@endsection



@section('scripts')
<script>
$(document).ready(function() {
    setTimeout(function() {
        $("#mensajeExito").fadeOut(1500);
    },3000);
});
</script>

@endsection