@extends('layouts.admin.main')
@section('contenido')  

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Alumno</h1>
            
          </div>
         




<form action="/admin/alumnos" enctype="multipart/form-data" method="post">

      @csrf

        <div class="modal-body">
            @if($message = Session::get('ErrorInsert'))

            <div class="col-12 alert alert-danger alert-dismissable fade show" role="alert">
              <h5>Errores:</h5>
            <ul>
              @foreach($errors->all() as $error)
            <li>{{ $error }}</li>

            @endforeach
            </ul>  
            </div>

            @endif
    
        <div class="form-group">
            <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{old('nombre')}}">
          </div>
          <div class="form-group">
          <textarea class="form-control"  placeholder="Descripción" id="descripcionAlumno" name="descripcionAlumno">{{old('descripcionAlumno')}}</textarea>
          </div>
          <div class="form-group">
          <input type="file"  class="form-control" name="imagen" id="imagen" size="30" >
          </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
</form>
</div>
  <!-- Curso Modal-->
  <div class="modal fade" id="alumnoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  
  <div class="row">
        @if($message = Session::get('Listo'))
          <div class="col-12 alert alert-success alert-dismissable fade show" id="mensajeExito" role="alert"> 
        <span>{{ $message }}</span>  
        </div>
        @endif
</div>
  <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar Testimonio</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">¿Desea agregar otro testimonio?</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Si</button>  
          <a type="button" class="btn btn-primary" href="/admin/alumnos">No</a>
          
          
        </div>
      </div>
    </div>
</div>
@endsection



@section('scripts')
<script>
$(document).ready(function() {
  @if($message = Session::get('Listo'))
    $("#alumnoModal").modal("show");
    @endif
   
    setTimeout(function() {
        $("#mensajeExito").fadeOut(1500);
        
    },3000);

   
});
</script>

@endsection