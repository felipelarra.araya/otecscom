
    <footer class="site-footer" role="contentinfo">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-4 mb-5">
            <h3>Sobre Nosotros</h3>
            <p class="mb-5">OTECSCOM es una empresa Regional, con inicio de actividades en junio del 2018, integrada por personal profesional y técnico altamente calificado y con una
amplia trayectoria laboral, quienes forman este Organismo Técnico Integral de Capacitación, cuya Política es brindar un servicio de capacitación de excelencia logrando satisfacer las necesidades y expectativas de nuestros clientes en las diversas áreas.
              </p>
            <ul class="list-unstyled footer-link d-flex footer-social">
              <li><a href="#" class="p-2"><span class="fa fa-twitter"></span></a></li>
              <li><a href="#" class="p-2"><span class="fa fa-facebook"></span></a></li>
              <li><a href="#" class="p-2"><span class="fa fa-linkedin"></span></a></li>
              <li><a href="#" class="p-2"><span class="fa fa-instagram"></span></a></li>
            </ul>

          </div>
          <div class="col-md-5 mb-5 pl-md-5">
            <h3>Contacto Info</h3>
            <ul class="list-unstyled footer-link">
              <li class="d-block">
                <span class="d-block">Dirección:</span>
                <span >Galería Los Carrera #572, 2do Piso Oficina 6, La Serena, Chile.</span></li>
              <li class="d-block"><span class="d-block">Teléfono:</span><span >+569 41538485 </span></li>
              <li class="d-block"><span class="d-block">Email:</span><span >informaciones@otecscom.cl</span></li>
            </ul>
          </div>
          <div class="col-md-3 mb-5">
            <h3>Menú</h3>
            <ul class="list-unstyled footer-link">
              <li><a href="psicotecnicas">Evaluaciones Psicotécnicas</a></li>
              <li><a href="capacitacion">Capacitación</a></li>
              <li><a href="acreditacion">Acreditación</a></li>
              <li><a href="escuelaConductores">Escuela de Conductores</a></li>
              <li><a href="contacto">Contacto</a></li>
            </ul>
          </div>
          <div class="col-md-3">
          
          </div>
        </div>
        <div class="row">
          <div class="col-12 text-md-center text-left">
             <p>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              Copyright &copy;
              <script>document.write(new Date().getFullYear());</script> All rights reserved 
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
          </div>
        </div>
      </div>
    </footer>
    <!-- END footer -->