<nav class="navbar navbar-expand-lg fixed-top">
        <div class="container">
            <a class="navbar-brand" href="/"><img src="{{ asset('images/logos/logo.jpg') }}" width=200></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                <a class="nav-link active" href="/">Inicio</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/capacitacion">Capacitación</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/acreditacion">&nbsp;Acreditación y<br>Compentencias</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/EvaluacionesPsicotecnicas">Evaluaciones<br>Psicotécnicas</a>
              </li>
              
              <li class="nav-item">
                <a class="nav-link" href="/escuelaConductores">Escuela de Conductores</a>
              </li>
              <!--<li class="nav-item">
                <a class="nav-link" href="#">Intranet</a>
              </li>-->
             
                </ul>
            </div>
        </div>
    </nav>


   