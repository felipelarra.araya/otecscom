@extends('layouts.main')
@section('title','Sobre Nosotros')

@section('contenido')

@foreach($about as $a)

    <section class="course_details_area section_gap">
        <div class="container">
            <div class="row">
                <div class="col-md-12 course_details_left mt-5">
                    
                    <div class="content_wrapper mt-2">
                        <h4 class="title text-center">{{ $a->nombre }}</h4>
                        <div class="content">
                        {!! $a->descripcion !!}
                    </div>

                    <div class="main_image">
                        <img class="img-fluid mx-auto d-block" width=400 src="{{ asset('images/sobreNosotros/'.$a->imagen) }}"alt="Curso Míneria La Serena Coquimbo">
                    </div>
                </div>


            </div>
        </div>
        @endforeach
    </section>
    <!-- END section -->

</div>  




@endsection