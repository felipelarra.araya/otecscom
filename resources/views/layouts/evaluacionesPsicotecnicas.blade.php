@extends('layouts.main')
@section('title','Evaluaciones Psicotécnicas')

@section('contenido')
<div class="inner-page">
@foreach($evaluacion as $e)
    <div class="slider-item" style="background-image: url('{{ asset('images/evaluaciones/'.$e->imagen) }}');">
        
        <div class="container">
          <div class="row slider-text align-items-center justify-content-center">
            <div class="col-md-8 text-center col-sm-12 element-animate pt-5">
              <h1 class="pt-5"><span></span>Evaluaciones Psicotécnicas</h1>
            </div>
          </div>
        </div>

      </div>

    </div>
    <section class="section border-t pb-0">
      <div class="container">
        <div class="row mb-5 element-animate">
          <div class="col-md-12 col-sm-12">
         
            <h2 class="heading text-center">{{ $e->nombre }}</h2>
            <p class="mb-2">{!! $e->descripcion !!}</p>
            
            
          </div>

          <div class="col-md-12 col-sm-12">
          <img src="{{ asset('images/evaluaciones/'.$e->imagen) }}" alt="Evaluaciones Psicotécnias La Serena" class="img-fluid mx-auto d-block">
            </div>
        </div>
      </div>
      @endforeach
      <hr style="width:75%;">
      
      <div class="container">
      <h2 class="heading col-12 text-center">Centro de Evaluaciones</h2>
      <div class="wrap">
		<div class="store-wrapper">
      <!--
			<div class="category_list">
				
  <div class="panel panel-default">
   List group 
  <ul class="list-group">
    <li class="list-group-item"><a href="#" class="category_item" category="all">Todo</a></li>
    <li class="list-group-item"><a href="#" class="category_item" category="prevencion">Prevención</a></li>
    <li class="list-group-item"><a href="#" class="category_item" category="mineria">Mineria</a></li>
    <li class="list-group-item"><a href="#" class="category_item" category="recursosHumanos">Recursos Humanos</a></li>

  </ul>
</div>
				
			</div>-->
			<section class="products-list2 mt-2 element-animate">
        @foreach($examenes as $examen)
				<div class="product-item col-md-4" category="prevencion">
					<img class="imgDetalle2" src="{{ asset('images/examenes/'.$examen->imagen) }}" alt="Exámenes Otecscom" >
					<a href="{{ url('detalleExamen/'.$examen->id.'/edit')}}">{{ $examen->nombre }}</a>
				</div>
				@endforeach
			</section>
		</div>
	</div>

</div>
      </div>
        
    </section>
    <!-- END section -->

    

</div>  

  
 
@endsection

@section('scripts')
<script src="{{ asset('/js/categorias.js')}}"></script>

@endsection