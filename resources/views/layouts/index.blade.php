@extends('layouts.main')

@section('contenido')  
    
<div class="top-shadow"></div>


    <!--  <div class="box-92819 shadow-lg">
        

          <div>
            <h1 class=" mb-3 text-black">Interior Design</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus quam, ratione earum.</p>
            <p class="mb-0 mt-4"><a href="#" class="btn btn-primary">Get In Touch</a></p>
          </div>

          

        
      </div>
-->

      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
   @foreach( $imagenes as $imagen )
      <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
   @endforeach
  </ol>
  <div class="carousel-inner">
  @foreach( $imagenes as $imagen )
       <div class="carousel-item {{ $loop->first ? 'active' : '' }}" >
           <img class="imag d-block w-100" src="{{ asset('images/sliders/'.$imagen->imagen) }}" alt="{{ $imagen->nombre }}">
           <div class="carousel-content">
           <h1 class=" mb-3 text-white">{{ $imagen->nombre }}</h1>
            <p style="font-size: 250%">{!!$imagen->descripcion !!}</p>

         </div>
               
       </div>
    @endforeach
    
  </div>
  
  <!--
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>-->
</div>       
@if($message = Session::get('Listo'))
          

<div class="modal fade" id="modalContacto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-sm modal-notify modal-danger" role="document">
    <!--Content-->
    <div class="modal-content text-center">
      <!--Header-->
      <div class="modal-header d-flex justify-content-center alert alert-success alert-dismissable">
        <p class="heading"><span>{{ $message }}</span> </p>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>

         
        @endif
<input type="button" value="Postular"  data-toggle="modal" data-target="#modalAgregar" class="d-block d-sm-block d-md-none btn btn-success btn-block">

<div class="card-img-overlay formSlider d-none d-sm-none d-md-block">
  
    <div class="container">
        <div class="row justify-content-end align-items-center">
            
            <div class="col-sm-4">
                <div class="card shadow-lg">
                  
                    <div class="card-body">
                    <div class="container">
    <div class="row">
        

        @if($message = Session::get('ErrorInsert'))

        <div class="col-12 alert alert-danger alert-dismissable fade show" role="alert">
          
        <ul>
          @foreach($errors->all() as $error)
        <li>{{ $error }}</li>

        @endforeach
        </ul>  
        </div>

        @endif
    
    </div>
</div>

                    <form action="/mail" method="post">
                      @csrf
                            <div class="form-group">
                                
                                <div class="form-group">
                                    <input type="text" class="form-control" name="nombre" placeholder="Nombre" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="email"  placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="telefono"  placeholder="Teléfono" required>
                                </div>
                                <div class=" form-group">
                                
                                <textarea name="mensaje" placeholder="Mensaje" id="message" class="form-control" cols="30" rows="3" required></textarea>
                              </div>
                                <input type="submit" value="Postular" class="btn btn-success btn-block">
                              

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


        


     
    


<!--

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
<div class="box-92819 shadow-lg">
        

        <form action="" method="post">
                  @csrf
                    <div class="row">
                    
                    <div class="col-md-6 form-group">
                        <label for="name">Nombre</label>
                        <input type="text" id="name" name="nombre" class="form-control ">
                      </div>
                    
                    <div class="col-md-6 form-group">
                        <label for="name">Teléfono</label>
                        <input type="text" id="name" name="telefono" class="form-control ">
                      </div>
                     
                    </div>
                  
                    <div class="row">
                      <div class="col-md-12 form-group">
                        <label for="email">Email</label>
                        <input type="email" id="email" name="email" class="form-control ">
                      </div>
                    </div>
                    <div class="row">
                    <div class="col-md-12 form-group">
                  <label for="exampleFormControlSelect1">Curso de Interés</label>
                  <select class="form-control" id="exampleFormControlSelect1">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
                    </div>
                    <div class="row">
                    <div class="col-md-12 form-group">
                  <label for="exampleFormControlSelect1">Situación Laboral</label>
                  <select class="form-control" id="exampleFormControlSelect1">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <input type="submit" value="Enviar Mensaje" class="btn btn-primary px-3 py-3">
                      </div>
                    </div>
                  </form>
                </div>
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="images/prueba4.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="images/prueba4.jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="images/prueba4.jpg" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>

  
</div>
-->
<!--
    <section class="home-slider owl-carousel">
      <div class="slider-item" style="background-image: url('images/hero_1.jpg');">
        <div class="container">
          <div class="row slider-text align-items-center justify-content-center">
            <div class="col-lg-7 text-center col-sm-12 element-animate">
              <h1 class="mb-4"><span>OTEC SCOM</span></h1>
              <p class="mb-5 w-75">Somos un Organismo de Capacitación Certificado en la ciudad de La Serena 
              Lo invitamos a conocer nuestros servicios de capacitación y certificación</p>
            </div>
          </div>
        </div>
      </div>

      <div class="slider-item" style="background-image: url('images/prueba3.jpg');">
        <div class="container">
          <div class="row slider-text align-items-center justify-content-center">
            <div class="col-lg-7 text-center col-sm-12 element-animate">
              <h1 class="mb-4"><span>Cursos de Capacitación</span></h1>
              <p class="mb-5 w-75">Tenemos nuevos cursos de capacitación en el área de Educación, Administración y Construcción.</p>
            </div>
          </div>
        </div>
      </div>

    </section>
    END slider 
-->
  <!--
<div class="box-92819 shadow-lg">
        

        <form action="" method="post">
                  @csrf
                    <div class="row">
                    
                    <div class="col-md-6 form-group">
                        <label for="name">Nombre</label>
                        <input type="text" id="name" name="nombre" class="form-control ">
                      </div>
                    
                    <div class="col-md-6 form-group">
                        <label for="name">Teléfono</label>
                        <input type="text" id="name" name="telefono" class="form-control ">
                      </div>
                     
                    </div>
                  
                    <div class="row">
                      <div class="col-md-12 form-group">
                        <label for="email">Email</label>
                        <input type="email" id="email" name="email" class="form-control ">
                      </div>
                    </div>
                    <div class="row">
                    <div class="col-md-12 form-group">
                  <label for="exampleFormControlSelect1">Curso de Interés</label>
                  <select class="form-control" id="exampleFormControlSelect1">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
                    </div>
                    <div class="row">
                    <div class="col-md-12 form-group">
                  <label for="exampleFormControlSelect1">Situación Laboral</label>
                  <select class="form-control" id="exampleFormControlSelect1">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <input type="submit" value="Enviar Mensaje" class="btn btn-primary px-3 py-3">
                      </div>
                    </div>
                  </form>
                </div>-->


                <!--================ Start Feature Area =================-->
    <section class="feature_area mt-4 element-animate">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-5">
            <div class="text-center">
              <h2 class="mb-3">Servicios</h2>
            </div>
          </div>
        </div>
        <div class="row">
          @foreach( $servicios as $servicio)
          <div class="col-lg-4 col-md-6">
            <div class="single_feature">
              <div class="icon"> <img src="images/drink.png"  alt="Image"></div>
              <div class="desc">
                <h4 class="mt-3 mb-2">{{$servicio->nombre}}</h4>
                <p>
                  {{$servicio->descripcion}}
                </p>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </section>
      

    <section class="section element-animate">
      <div class="container">
        <div class="row align-items-center mb-5">
          <div class="col-lg-7 order-md-2">
          @foreach($about as $a)
            <div class=""><img src="{{ asset('images/sobreNosotros/'.$a->imagen) }}" width="400" height="100"  alt="Quienes somos OtecsCom" class="img-fluid d-none d-md-block d-none d-sm-block"></div>
          </div>
          <div class="col-md-5 pr-md-5 mb-5">
            
            <div class="">
            <h3>{{ $a->nombre }}</h3>
              <div class="">
                <p>{!! str_limit($a->descripcion, 358) !!}</p>
                <p><a href="/sobreNosotros" class="readmore">Ver Más <span class="ion-android-arrow-dropright-circle"></span></a></p>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </section>




   

    

     	<!-- Courses -->
<div id="courses">

<!-- container -->
<div class="container sectionCursos element-animate">

  <!-- row -->
  <div class="row">
    <div class="section-header col-md-12 text-center">
      <h2>Nuestros Cursos</h2>
      
    </div>
  </div>
  <section class="products-list2 mt-2">

@foreach($categorias as $categoria)
  <div class="product-item col-md-4">
  <img class="imgDetalle2" src="{{ asset('images/categoriasCursos/'.$categoria->imagen) }}" alt="Cursos OtecSom La Serena Chile" >
    <a href="capacitacion">{{$categoria->nombre}}</a>
    
  </div>
  @endforeach
</section>

</div>
<!-- container -->

</div>
<!-- /Courses -->
    


    

  

    
    
    
   

  <!--
    <section class="section border-t pb-0">
      <div class="container">
        <div class="row justify-content-center mb-5 element-animate">
          <div class="col-md-8 text-center">
            <h2 class=" heading mb-4">Our Latest Projects</h2>
            <p class="mb-5 lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="row no-gutters">
          <div class="col-md-4 element-animate">
            <a href="project-single.html" class="link-thumbnail">
              <h3>Ducting Design in Colorado</h3>
              <span class="ion-plus icon"></span>
              <img src="images/img_1.jpg" alt="Image" class="img-fluid">
            </a>
          </div>
          <div class="col-md-4 element-animate">
            <a href="project-single.html" class="link-thumbnail">
              <h3>Tanks Project In California</h3>
              <span class="ion-plus icon"></span>
              <img src="images/img_2.jpg" alt="Image" class="img-fluid">
            </a>
          </div>
          <div class="col-md-4 element-animate">
            <a href="project-single.html" class="link-thumbnail">
              <h3>Structural Design in New York</h3>
              <span class="ion-plus icon"></span>
              <img src="images/img_3.jpg" alt="Image" class="img-fluid">
            </a>
          </div>
          <div class="col-md-4 element-animate">
            <a href="project-single.html" class="link-thumbnail">
              <h3>Stacks Design</h3>
              <span class="ion-plus icon"></span>
              <img src="images/img_4.jpg" alt="Image" class="img-fluid">
            </a>
          </div>
          <div class="col-md-4 element-animate">
            <a href="project-single.html" class="link-thumbnail">
              <h3>Intercate Custom</h3>
              <span class="ion-plus icon"></span>
              <img src="images/img_1.jpg" alt="Image" class="img-fluid">
            </a>
          </div>
          <div class="col-md-4 element-animate">
            <a href="project-single.html" class="link-thumbnail">
              <h3>Banker Design</h3>
              <span class="ion-plus icon"></span>
              <img src="images/img_2.jpg" alt="Image" class="img-fluid">
            </a>
          </div>
        </div>
        
      </div>
    </section>
-->
    <!-- END section -->

   
    <section class="section bg-light block-11">
      <div class="container"> 
        <div class="row justify-content-center mb-5">
          <div class="col-md-8 text-center">
            <h2 class=" heading mb-4">Nuestros Alumnos</h2>
          </div>
        </div>
        <div class="nonloop-block-11 owl-carousel">
          @foreach($alumnos as $alumno)
          <div class="item">
            <div class="block-33 h-100">
                <div class="vcard d-flex mb-3">
                  <div class="image align-self-center"><img src="{{ asset('images/alumnos/'.$alumno->imagen) }}" alt="Person here"></div>
                  <div class="name-text align-self-center">
                    <h2 class="heading">{{$alumno->nombre}}</h2>
                  </div>
                </div>
                <div class="text">
                  <blockquote>
                    <p>&rdquo;{{$alumno->descripcion}} &ldquo;</p>
                  </blockquote>
                </div>
              </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>

    
  </section>



  <!-- Modal Agregar -->
<div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Postula con nosotros</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="container">
    <div class="row">
        @if($message = Session::get('Listo'))
          <div class="col-12 alert alert-success alert-dismissable fade show" id="mensajeExitoModal" role="alert"> 
         
        <span>{{ $message }}</span>  
        </div>
        @endif

        @if($message = Session::get('ErrorInsert'))

        <div class="col-12 alert alert-danger alert-dismissable fade show" role="alert">
          
        <ul>
          @foreach($errors->all() as $error)
        <li>{{ $error }}</li>

        @endforeach
        </ul>  
        </div>

        @endif
    
    </div>
</div>

                    <form action="/mail" method="post">
                      @csrf
                            
                                
                                <div class="form-group col-sm-12 mt-3">
                                    <input type="text" class="form-control" name="nombre" placeholder="Nombre" required>
                                </div>
                                <div class="form-group col-sm-12">
                                    <input type="text" class="form-control" name="email"  placeholder="Email" required>
                                </div>
                                <div class="form-group col-sm-12">
                                    <input type="text" class="form-control" name="telefono"  placeholder="Teléfono" required>
                                </div>
                                <div class=" form-group col-sm-12">
                                
                                <textarea name="mensaje" placeholder="Mensaje" id="message" class="form-control" cols="30" rows="3" required></textarea>
                              </div>
                              <div class=" form-group col-sm-12">
                                <input type="submit" value="Postular" class="btn btn-success btn-block ">
</div>

                            </div>
                        </form>
                    </div>
    </div>
  </div>



@endsection

@section('scripts')
    <script>
    $(document).ready(function() {
      @if($message = Session::get('ErrorInsert'))
    $("#modalAgregar").modal("show");
    @endif

    @if($message = Session::get('Listo'))
    $("#modalContacto").modal("show");
    @endif
    
    setTimeout(function() {
      $("#modalContacto").modal("hide");
        },2000);
      
        setTimeout(function() {
            $("#mensajeExito").fadeOut(1500);
        },3000);

      
    });
    </script>

@endsection