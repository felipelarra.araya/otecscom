<!DOCTYPE html>
<html lang="es">
  <head>
    <title>@yield('title','otecscom.cl')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="{{ asset('images/logos/logo.jpg')}}" rel="icon">
    
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Oxygen:400,700" rel="stylesheet">

    <link rel="stylesheet" href=" {{ asset('/css/animate.css')}}">
    <link rel="stylesheet" href="{{ asset('/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/css/jquery.fancybox.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/css/_bootstrap.css')}}">

    <link rel="stylesheet" href="{{ asset('/fonts/ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/fonts/fontawesome/css/font-awesome.min.css')}}">

    <!-- Theme Style -->
    <link rel="stylesheet" href="{{ asset('/css/style.css')}}">
    @yield('css')
    </head>

    <body>
    @include('layouts.navBar')
  

    @yield('contenido')


   
    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

    <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

    @include('layouts.footer')
    <script src="{{ asset('/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{ asset('/js/popper.min.js')}}"></script>
    <script src="{{ asset('/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('/js/jquery.waypoints.min.js')}}"></script>
    <script src="{{ asset('/js/jquery.fancybox.min.js')}}"></script>
    <script src="{{ asset('/js/main.js')}}"></script>
    <script src="{{ asset('/js/jquery.easing.min.js')}}"></script>

    

    

    @yield('scripts')
    
  </body>
</html>