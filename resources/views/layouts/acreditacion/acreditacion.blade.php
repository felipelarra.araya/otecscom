@extends('layouts.main')
@section('title','Acreditación')

@section('contenido')
<div class="inner-page">
  @foreach($acreditacion as $a)
    <div class="slider-item" style="background-image: url('{{ asset('images/acreditacion/'.$a->imagen) }}');">
        
        <div class="container">
          <div class="row slider-text align-items-center justify-content-center">
            <div class="col-md-8 text-center col-sm-12 element-animate pt-5">
              <h1 class="pt-5"><span></span>{{ $a->nombre }}</h1>
            </div>
          </div>
        </div>

      </div>

    </div>
    <section class="section border-t pb-0">
      <div class="container">
        <div class="row  mb-5 element-animate">
          <div class="col-md-6 col-sm-12">
            <h2 class="heading">{{ $a->nombre }}</h2>
           
            <p class="mb-2">{!! $a->descripcion !!}</p>
            
          </div>
         

          <div class="col-md-6 col-sm-12">
          <img src="{{ asset('images/acreditacion/'.$a->imagen) }}" alt="Acreditación cursos La Serena minería" class="img-fluid">
            </div>
        </div>
      </div>
      @endforeach
      <hr style="width:75%;">

      
    </section>
    <!-- END section -->

    

</div>  

  
 
@endsection

@section('scripts')
<script src="{{ asset('/js/categorias.js')}}"></script>

@endsection