@extends('layouts.main')
@section('title','Detalle Curso')

@section('contenido')

<div class="inner-page">

    <div class="slider-item" style="background-image: url('{{ asset('images/cursos/'.$curso->imagen) }}');">
        
        <!--<div class="container">
          <div class="row slider-text align-items-center justify-content-center">
            <div class="col-md-8 text-center col-sm-12 element-animate pt-5">
              <h1 class="pt-5"><span></span>Capacitación</h1>
            </div>
          </div>
        </div>-->

      </div>

    </div>



<!--================ Start Course Details Area =================-->
 <section class="course_details_area section_gap">
        <div class="container">
            <div class="row">
                <div class="col-md-12 course_details_left mt-5">
                    <div class="main_image">
                        <img class="img-fluid mx-auto d-block" src="{{ asset('images/cursos/'.$curso->imagen) }}"alt="Curso Míneria La Serena Coquimbo">
                    </div>
                    <div class="content_wrapper mt-2">
                        <h4 class="title text-center">{{ $curso->nombre }}</h4>
                        
                        <div class="content">
                        <div class="row">
                        <div class="col-md-12">
                        {!! $curso->descripcion !!}
                        @if($curso->id == 2 )
                        <video class="mx-auto d-block img-fluid" src="{{ asset('videos/videocargador frontal.mp4') }}" width=620  height=460 controls>
                        Lo sentimos. Este vídeo no puede ser reproducido en tu navegador.<br>
                        La versión descargable está disponible en <a href="URL">Enlace</a>. 
                        </video>
                        @endif

                        @if($curso->id == 6 )
                        <video class="mx-auto d-block img-fluid" src="{{ asset('videos/videogruahorquilla.mp4') }}"width=620  height=460 controls>
                        Lo sentimos. Este vídeo no puede ser reproducido en tu navegador.<br>
                        La versión descargable está disponible en <a href="URL">Enlace</a>. 
                        </video>
                        @endif

                        @if($curso->id == 1 )
                        <video class="mx-auto d-block img-fluid" src="{{ asset('videos/videoretroexcavadora.mp4') }}" width=620  height=460  controls>
                        Lo sentimos. Este vídeo no puede ser reproducido en tu navegador.<br>
                        La versión descargable está disponible en <a href="URL">Enlace</a>. 
                        </video>
                        @endif

</div>
</div>

                    </div>
                   
                </div>


            </div>
        </div>
    </section>
    <!--================ End Course Details Area =================-->

    @endsection