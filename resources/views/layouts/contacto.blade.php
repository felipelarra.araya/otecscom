@extends('layouts.main')
@section('title','Contacto')

@section('contenido')

<div class="inner-page">
    <div class="slider-item" style="background-image: url('images/hero_2.jpg');">
        
        <div class="container">
          <div class="row slider-text align-items-center justify-content-center">
            <div class="col-md-8 text-center col-sm-12 element-animate pt-5">
              <h1 class="pt-5"><span>Contáctanos</span></h1>
              
            </div>
          </div>
        </div>

      </div>
    </div>
    <!-- END slider -->
    </div>

    
    <section class="section border-bottom">

    <div class="container">
    <div class="row">
        @if($message = Session::get('Listo'))
          <div class="col-12 alert alert-success alert-dismissable fade show" id="mensajeExito" role="alert"> 
         
        <span>{{ $message }}</span>  
        </div>
        @endif

        @if($message = Session::get('ErrorInsert'))

        <div class="col-12 alert alert-danger alert-dismissable fade show" role="alert">
          
        <ul>
          @foreach($errors->all() as $error)
        <li>{{ $error }}</li>

        @endforeach
        </ul>  
        </div>

        @endif
    
    </div>
    
    
    <div class="container">
        <div class="row">
          <div class="col-md-6 mb-5 order-2">
            <form action="/mail" method="post">
            @csrf
              <div class="row">
              
              <div class="col-md-12 form-group">
                  <label for="name">Asunto</label>
                  <input type="text" id="name" name="asunto" class="form-control" required>
                </div>
              
              <div class="col-md-12 form-group">
                  <label for="name">Nombre</label>
                  <input type="text" id="name" name="nombre" class="form-control" required>
                </div>
               
              </div>
              <div class="row">
                <div class="col-md-12 form-group">
                  
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 form-group">
                  <label for="email">Email</label>
                  <input type="email" id="email" name="email" class="form-control" required>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 form-group">
                  <label for="message">Mensaje</label>
                  <textarea name="mensaje" id="message" class="form-control" cols="30" rows="8" required></textarea>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 form-group">
                  <input type="submit" value="Enviar Mensaje" class="btn btn-primary px-3 py-3">
                </div>
              </div>
            </form>
          </div>
          <div class="col-md-6 order-2 mb-5">
            <div class="row justify-content-right">
              <div class="col-md-8 mx-auto contact-form-contact-info">
                <p class="d-flex">
                  <span class="ion-ios-location icon mr-5"></span>
                  <span>Galería Los Carrera #572, 2do Piso Oficina 6, La Serena, Chile.</span>
                </p>

                <p class="d-flex">
                  <span class="ion-ios-telephone icon mr-5"></span>
                  <span>+569 41538485</span>
                </p>

                <p class="d-flex">
                  <span class="ion-android-mail icon mr-5"></span>
                  <span>informaciones@otecscom.cl</span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

   
    <!-- END .block-4 -->
    </section>

    @endsection

    @section('scripts')
    <script>
    $(document).ready(function() {
        
      
        setTimeout(function() {
            $("#mensajeExito").fadeOut(1500);
        },3000);

      
    });
    </script>

@endsection