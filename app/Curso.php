<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Curso extends Model
{
    use SoftDeletes; 
    protected $fillable = ['nombre','descripcion','id_categoria','imagen'];
    protected $dates = ['deleted_at'];
    protected $hidden =   ['created_at','updated_at'];
}
