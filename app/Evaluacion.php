<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Evaluacion extends Model
{
    
    protected $table = "evaluaciones";
    protected $fillable = ['nombre','descripcion','imagen'];
    protected $hidden =   ['created_at','updated_at'];
}
