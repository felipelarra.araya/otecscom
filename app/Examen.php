<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Examen extends Model
{
    use SoftDeletes; 
    protected $dates = ['deleted_at'];
    protected $table = "examenes";
    protected $fillable = ['nombre','descripcion','imagen','active'];
}
