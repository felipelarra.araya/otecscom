<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Servicio extends Model
{
    use SoftDeletes; 
    protected $dates = ['deleted_at'];
    protected $table = "servicios";
    protected $fillable = ['nombre','descripcion','imagen','active'];
}
