<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SobreNosotros extends Model
{
    use SoftDeletes; 
    protected $dates = ['deleted_at'];
    protected $table = "sobreNosotros";
    protected $fillable = ['nombre','descripcion','imagen'];
    
}
