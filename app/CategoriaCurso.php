<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoriaCurso extends Model
{
    use SoftDeletes;  

    protected $dates = ['deleted_at'];
    protected $table = "categoriaCursos";
    protected $fillable = ['nombre','imagen'];
    protected $hidden =   ['created_at','updated_at'];
}
