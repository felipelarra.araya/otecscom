<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acreditacion extends Model
{
    
    
    protected $table = "acreditaciones";
    protected $fillable = ['nombre','descripcion','imagen'];
}
