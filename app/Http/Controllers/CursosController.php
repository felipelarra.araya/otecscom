<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Curso;
use Validator;
use Image;
use App\CategoriaCurso;
class CursosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index(){

        $cursos = \DB::table('cursos')
                    ->select('cursos.*')
                    ->whereNull('deleted_at')
                    ->orderBy('id','DESC')
                    ->paginate(3);

                    
        return view('layouts.admin.capacitacion.cursos')->with('cursos',$cursos); 
    }

    public function getCursoAdd()
    {

        $categorias = CategoriaCurso::all();
        return view('layouts.admin.capacitacion.addCurso', compact('categorias'));
        //return view('layouts.admin.capacitacion.addCurso')->with('data',$data);
    }

    public function edit($id){   
        $categorias = CategoriaCurso::all();
        $curso = Curso::find($id);
        return view('layouts.admin.capacitacion.cursoEdit',compact('curso','categorias')); 
    }

    public function editarCurso(Request $request)
    {
        
        $curso = Curso::find($request->id);
        $imagenPrevia = $curso->imagen;
        $validator = Validator::make($request->all(),[
            'nombre'=>'required|min:3',
            'imagen'=>'image|mimes:jpg,jpeg,png,gif,svg',
            'descripcion'=>'required',
            'categoria'=>'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }else{


            if ($request->hasFile('imagen')){
                $imagen = $request->file('imagen');
                $nombreFoto = time().'.'.$imagen->getClientOriginalExtension();
                $destino = public_path('images/cursos');
                $request->imagen->move($destino, $nombreFoto);
                $red = Image::make($destino.'/'.$nombreFoto);
                $red->resize(200,null, function($constraint){
                $constraint->aspectRatio();
            });
            $red->save($destino.'/thumbs/'.$nombreFoto);
                unlink($destino.'/'.$imagenPrevia);
                unlink($destino.'/thumbs/'.$imagenPrevia);
                $curso->imagen=$nombreFoto; 
    
              }
              $curso->nombre= $request->nombre;
              $curso->descripcion= $request->descripcion;
              $curso->id_categoria= $request->categoria;
              $curso->save();
              return redirect('/admin/cursos')->with('Listo','Registro actualizado exitosamente');
           
        }
    }



    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'nombre'=>'required|min:3',
            'imagen'=>'required|image|mimes:jpg,jpeg,png,gif,svg',
            'descripcion'=>'required',
            'categoria'=>'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }
        else{
            
            $imagen = $request->file('imagen');
            $nombreImagen = time().'.'.$imagen->getClientOriginalExtension();
            $destino = public_path('images/cursos');
            $request->imagen->move($destino, $nombreImagen);
            $red = Image::make($destino.'/'.$nombreImagen);
            $red->resize(200,null, function($constraint){
                $constraint->aspectRatio();
            });
            $red->save($destino.'/thumbs/'.$nombreImagen);
            
            $curso = Curso::create([
                'nombre'=>$request->nombre,
                'descripcion'=>$request->descripcion,
                'id_categoria'=>$request->categoria,
                'imagen'=>$nombreImagen
            ]);
               

            return back()->with('Listo','Ingresado correctamente');
        
        }
        
    }

    public function getCursoDelete($id)
    {
        
        $curso = Curso::find($id);
        $imagenPrevia = $curso->imagen;
        if($curso->delete()){
            $destino = public_path('images/cursos');
            unlink($destino.'/'.$imagenPrevia);
            unlink($destino.'/thumbs/'.$imagenPrevia);
            return back()->with('Listo','Registro eliminado exitosamente');
        }
    }
}
