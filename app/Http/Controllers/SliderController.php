<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use Validator;
use Image;

class SliderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $sliders = \DB::table('sliders')
                    ->select('sliders.*')
                    ->whereNull('deleted_at')
                    ->orderBy('id','DESC')
                    ->paginate(3);

        return view('layouts.admin.slider.sliders')->with('sliders',$sliders); 
    }


    public function getSliderAdd()
    {

        $sliders = Slider::all();
        return view('layouts.admin.slider.addSlider', compact('sliders'));
        //return view('layouts.admin.capacitacion.addCurso')->with('data',$data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
      
            'imagen'=>'required|image|mimes:jpg,jpeg,png,gif,svg'
           
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }
        else{
            
            $imagen = $request->file('imagen');
            $nombreImagen = time().'.'.$imagen->getClientOriginalExtension();
            $destino = public_path('images/sliders');
            $request->imagen->move($destino, $nombreImagen);
            $red = Image::make($destino.'/'.$nombreImagen);
            $red->resize(200,null, function($constraint){
                $constraint->aspectRatio();
            });
            $red->save($destino.'/thumbs/'.$nombreImagen);
            
            $slider = Slider::create([
                'nombre'=>$request->nombre,
                'descripcion'=>$request->descripcion,
                'imagen'=>$nombreImagen
            ]);
               

            return back()->with('Listo','Ingresado correctamente');
        
        }
        
    }


    public function editarSlider(Request $request)
    {
        
        $slider = Slider::find($request->id);
        $imagenPrevia = $slider->imagen;
        $validator = Validator::make($request->all(),[
            'imagen'=>'image|mimes:jpg,jpeg,png,gif,svg'
            
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }else{


            if ($request->hasFile('imagen')){
                $imagen = $request->file('imagen');
                $nombreFoto = time().'.'.$imagen->getClientOriginalExtension();
                $destino = public_path('images/sliders');
                $request->imagen->move($destino, $nombreFoto);
                $red = Image::make($destino.'/'.$nombreFoto);
                $red->resize(200,null, function($constraint){
                $constraint->aspectRatio();
            });
            $red->save($destino.'/thumbs/'.$nombreFoto);
                unlink($destino.'/'.$imagenPrevia);
                unlink($destino.'/thumbs/'.$imagenPrevia);
                $slider->imagen=$nombreFoto; 
    
              }
              $slider->nombre= $request->nombre;
              $slider->descripcion= $request->descripcion;
              $slider->save();
              return redirect('/admin/sliders')->with('Listo','Registro actualizado exitosamente');
           
        }
    }

    public function edit($id){   
           
        $slider = Slider::find($id);
        return view('layouts.admin.slider.editSlider')->with('slider',$slider); 

    }


    public function getSliderDelete($id)
    {
        
        $slider = Slider::find($id);
        $imagenPrevia = $slider->imagen;
        if($slider->delete()){
            $destino = public_path('images/sliders');
            unlink($destino.'/'.$imagenPrevia);
            unlink($destino.'/thumbs/'.$imagenPrevia);
            return back()->with('Listo','Registro eliminado exitosamente');
        }
    }

}
