<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Acreditacion;
use Validator;
use Image;

class AcreditacionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $acreditacion = \DB::table('acreditaciones')
                    ->select('acreditaciones.*')
                    ->orderBy('id','DESC')
                    ->get();
        return view('layouts.admin.acreditacion.acreditacion')->with('acreditacion',$acreditacion); 
    }

    public function getAcreditacionEdit($id){   
           
        $acreditacion = Acreditacion::find($id);
        return view('layouts.admin.acreditacion.editAcreditacion')->with('acreditacion',$acreditacion); 

    }

    public function editarAcreditacion(Request $request)
    {
        
        $acreditacion = Acreditacion::find($request->id);
        $imagenPrevia = $acreditacion->imagen;
        $validator = Validator::make($request->all(),[
            'nombre'=>'required|min:3',
            'imagen'=>'image|mimes:jpg,jpeg,png,gif,svg',
            'descripcion'=>'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }else{


            if ($request->hasFile('imagen')){
                $imagen = $request->file('imagen');
                $nombreFoto = time().'.'.$imagen->getClientOriginalExtension();
                $destino = public_path('images/acreditacion');
                $request->imagen->move($destino, $nombreFoto);
                $red = Image::make($destino.'/'.$nombreFoto);
                $red->resize(200,null, function($constraint){
                $constraint->aspectRatio();
            });
                $red->save($destino.'/thumbs/'.$nombreFoto);
                unlink($destino.'/'.$imagenPrevia);
                unlink($destino.'/thumbs/'.$imagenPrevia);
                $acreditacion->imagen=$nombreFoto; 
    
              }

              $acreditacion->nombre= $request->nombre;
              $acreditacion->descripcion= $request->descripcion;
              $acreditacion->save();
              return redirect('/admin/acreditacion')->with('Listo','Registro actualizado exitosamente');
            
           
        }
    }
}
