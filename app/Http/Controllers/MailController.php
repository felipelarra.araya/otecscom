<?php

namespace App\Http\Controllers;
use App\Mail\MessageReceived;
use Mail;
use Validator;


use Illuminate\Http\Request;




class MailController extends Controller
{
    public function store(Request $request)
    {

        
        
        $validator = Validator::make($request->all(),[
            'nombre'=>'required',
            'email'=>'required|',
            'mensaje'=>'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }else{
        

        Mail::send('layouts.emails.messageReceived', $request->all(), function($msj){
            
            $msj->subject('OtecsCom Web Contacto');
            $msj->to('felipelarra.araya@gmail.com');
        });
        return back()->with('Listo','Ingresado correctamente');

    }
       
    }
}
