<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Capacitacion;
use Validator;
use Image;


class CapacitacionController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }


   public function index()
   {
    $capacitacion = \DB::table('capacitaciones')
    ->select('capacitaciones.*')
    ->orderBy('id','DESC')
    ->get();

    return view('layouts.admin.capacitacion.capacitacion')->with('capacitacion',$capacitacion); 
   }

   public function edit($id){   
           
    $capacitacion = Capacitacion::find($id);
    return view('layouts.admin.capacitacion.capacitacionEdit')->with('capacitacion',$capacitacion); 

    }

    public function editarCapacitacion(Request $request)
    {
        
        $capacitacion = Capacitacion::find($request->id);
        $imagenPrevia = $capacitacion->imagen;
        $validator = Validator::make($request->all(),[
            'nombre'=>'required|min:3',
            'imagen'=>'image|mimes:jpg,jpeg,png,gif,svg',
            'descripcion'=>'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }else{


            if ($request->hasFile('imagen')){
                $imagen = $request->file('imagen');
                $nombreFoto = time().'.'.$imagen->getClientOriginalExtension();
                $destino = public_path('images/capacitacion');
                $request->imagen->move($destino, $nombreFoto);
                $red = Image::make($destino.'/'.$nombreFoto);
                $red->resize(200,null, function($constraint){
                $constraint->aspectRatio();
            });
            $red->save($destino.'/thumbs/'.$nombreFoto);
                unlink($destino.'/'.$imagenPrevia);
                unlink($destino.'/thumbs/'.$imagenPrevia);
                $capacitacion->imagen=$nombreFoto; 
    
              }
              $capacitacion->nombre= $request->nombre;
              $capacitacion->descripcion= $request->descripcion;
              $capacitacion->save();
              return redirect('/admin/capacitacion')->with('Listo','Registro actualizado exitosamente');
           
        }
    }
}
