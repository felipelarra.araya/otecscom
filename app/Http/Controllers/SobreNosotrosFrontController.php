<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SobreNosotrosFrontController extends Controller
{
    public function index()
    {
        $about = \DB::table('sobreNosotros')
        ->select('sobreNosotros.*')
        ->orderBy('id','DESC')
        ->get();

        return view('layouts.sobreNosotros.sobreNosotros')->with('about',$about); 
    }
}
