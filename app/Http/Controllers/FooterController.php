<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FooterController extends Controller
{

    public function index()
    {
        $about = \DB::table('sobreNosotros')
        ->select('sobreNosotros.*')
        ->orderBy('id','DESC')
        ->get();
    
        return view('layouts.footer')->with('about',$about); 
    }
    
}
