<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Escuela;
use Validator;
use Image;

class EscuelaConductoresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $escuela = \DB::table('escuelas')
                    ->select('escuelas.*')
                    ->orderBy('id','DESC')
                    ->get();
        return view('layouts.admin.escuelaConductores.escuelaConductores')->with('escuela',$escuela); 
    }

    public function getEscuelaEdit($id){   
           
        $escuela = Escuela::find($id);
        return view('layouts.admin.escuelaConductores.editEscuelaConductores')->with('escuela',$escuela); 

    }

    public function editarEscuelaConductores(Request $request)
    {
        
        $escuela = Escuela::find($request->id);
        $imagenPrevia = $escuela->imagen;
        $validator = Validator::make($request->all(),[
            'nombre'=>'required|min:3',
            'imagen'=>'image|mimes:jpg,jpeg,png,gif,svg',
            'descripcion'=>'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }else{


            if ($request->hasFile('imagen')){
                $imagen = $request->file('imagen');
                $nombreFoto = time().'.'.$imagen->getClientOriginalExtension();
                $destino = public_path('images/escuelaConductores');
                $request->imagen->move($destino, $nombreFoto);
                $red = Image::make($destino.'/'.$nombreFoto);
                $red->resize(200,null, function($constraint){
                $constraint->aspectRatio();
            });
                $red->save($destino.'/thumbs/'.$nombreFoto);
                unlink($destino.'/'.$imagenPrevia);
                unlink($destino.'/thumbs/'.$imagenPrevia);
                $escuela->imagen=$nombreFoto; 
    
              }

              $escuela->nombre= $request->nombre;
              $escuela->descripcion= $request->descripcion;
              $escuela->save();
              return redirect('/admin/escuelaConductores')->with('Listo','Registro actualizado exitosamente');
            
           
        }
    }

}
