<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Servicio;
use Validator;
use Image;


class ServiciosController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }



    public function index()
    {
        $servicios = \DB::table('servicios')
                    ->select('servicios.*')
                    ->whereNull('deleted_at')
                    ->orderBy('id','DESC')
                    ->paginate(3);

                    
        return view('layouts.admin.servicios.servicios')->with('servicios',$servicios); 
    }

    public function getServicioAdd()
    {

        $servicio = Servicio::all();
        return view('layouts.admin.servicios.addServicio', compact('servicio'));
        //return view('layouts.admin.capacitacion.addCurso')->with('data',$data);
    }

    public function edit($id){   
        $servicio = Servicio::find($id);
        return view('layouts.admin.servicios.servicioEdit',compact('servicio')); 
    }

    public function editarServicio(Request $request)
    {
        
        $servicio = Servicio::find($request->id);
        $imagenPrevia = $servicio->imagen;
        $validator = Validator::make($request->all(),[
            'nombre'=>'required|min:3',
            'imagen'=>'image|mimes:jpg,jpeg,png,gif,svg',
            'descripcionServicio'=>'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }else{


            if ($request->hasFile('imagen')){
                $imagen = $request->file('imagen');
                $nombreFoto = time().'.'.$imagen->getClientOriginalExtension();
                $destino = public_path('images/servicios');
                $request->imagen->move($destino, $nombreFoto);
                $red = Image::make($destino.'/'.$nombreFoto);
                $red->resize(200,null, function($constraint){
                $constraint->aspectRatio();
            });
            $red->save($destino.'/thumbs/'.$nombreFoto);
                unlink($destino.'/'.$imagenPrevia);
                unlink($destino.'/thumbs/'.$imagenPrevia);
                $servicio->imagen=$nombreFoto; 
    
              }
              $servicio->nombre= $request->nombre;
              $servicio->descripcion= $request->descripcionServicio;
              $servicio->save();
              //return back()->with('Listo','Registro actualizado exitosamente');
              return redirect('/admin/servicios')->with('Listo','Registro actualizado exitosamente');
           
        }
    }



    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'nombre'=>'required|min:3',
            'imagen'=>'required|image|mimes:jpg,jpeg,png,gif,svg',
            'descripcionServicio'=>'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }
        else{
            
            $imagen = $request->file('imagen');
            $nombreImagen = time().'.'.$imagen->getClientOriginalExtension();
            $destino = public_path('images/servicios');
            $request->imagen->move($destino, $nombreImagen);
            $red = Image::make($destino.'/'.$nombreImagen);
            $red->resize(200,null, function($constraint){
                $constraint->aspectRatio();
            });
            $red->save($destino.'/thumbs/'.$nombreImagen);
            
            $servicio = Servicio::create([
                'nombre'=>$request->nombre,
                'descripcion'=>$request->descripcionServicio,
                'imagen'=>$nombreImagen
            ]);
               

            return back()->with('Listo','Ingresado correctamente');
        
        }
        
    }

    public function getServicioDelete($id)
    {
        
        $servicio = Servicio::find($id);
        $imagenPrevia = $servicio->imagen;
        if($servicio->delete()){
            $destino = public_path('images/servicios');
            unlink($destino.'/'.$imagenPrevia);
            unlink($destino.'/thumbs/'.$imagenPrevia);
            return back()->with('Listo','Registro eliminado exitosamente');
        }
    }
}

