<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Curso;

class CapacitacionFrontController extends Controller
{
    public function index()
    {
        $categorias = \DB::table('categoriaCursos')
                    ->select('categoriaCursos.*')
                    ->whereNull('deleted_at')
                    ->orderBy('id','DESC')
                    ->get();

        $cursosCategoria = \DB::table('cursos')
                    ->join('categoriaCursos','categoriaCursos.id' , '=' ,'cursos.id_categoria')
                    ->select('cursos.id','cursos.nombre','cursos.imagen','categoriaCursos.nombre as categoriaNombre')
                    ->whereNull('cursos.deleted_at')
                    ->get();

        $capacitacion = \DB::table('capacitaciones')
                        ->select('capacitaciones.*')
                        ->orderBy('id','DESC')
                        ->get();

                    

        return view("layouts.capacitacion", 
                    ['categorias' => $categorias, 
                    'cursosCategoria' => $cursosCategoria,
                    'capacitacion' => $capacitacion]);

    }

    public function edit($id){   
           
        $curso = Curso::find($id);
        return view('layouts.detalleCurso')->with('curso',$curso); 
    
        }



}
