<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alumno;
use Validator;
use Image;

class AlumnosController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $alumnos = \DB::table('alumnos')
                    ->select('alumnos.*')
                    ->whereNull('deleted_at')
                    ->orderBy('id','DESC')
                    ->paginate(3);

                    
        return view('layouts.admin.alumnos.alumnos')->with('alumnos',$alumnos); 
    }

    public function getAlumnoAdd()
    {

        $alumno = Alumno::all();
        return view('layouts.admin.alumnos.addAlumno', compact('alumno'));
    }

    public function edit($id){   
        $alumno = Alumno::find($id);
        return view('layouts.admin.alumnos.alumnoEdit',compact('alumno')); 
    }

    public function editarAlumno(Request $request)
    {
        
        $alumno = Alumno::find($request->id);
        $imagenPrevia = $alumno->imagen;
        $validator = Validator::make($request->all(),[
            'nombre'=>'required|min:3',
            'imagen'=>'image|mimes:jpg,jpeg,png,gif,svg',
            'descripcionAlumno'=>'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }else{


            if ($request->hasFile('imagen')){
                $imagen = $request->file('imagen');
                $nombreFoto = time().'.'.$imagen->getClientOriginalExtension();
                $destino = public_path('images/alumnos');
                $request->imagen->move($destino, $nombreFoto);
                $red = Image::make($destino.'/'.$nombreFoto);
                $red->resize(200,null, function($constraint){
                $constraint->aspectRatio();
            });
            $red->save($destino.'/thumbs/'.$nombreFoto);
                unlink($destino.'/'.$imagenPrevia);
                unlink($destino.'/thumbs/'.$imagenPrevia);
                $alumno->imagen=$nombreFoto; 
    
              }
              $alumno->nombre= $request->nombre;
              $alumno->descripcion= $request->descripcionAlumno;
              $alumno->save();
              return redirect('/admin/alumnos')->with('Listo','Registro actualizado exitosamente');
           
        }
    }



    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'nombre'=>'required|min:3',
            'imagen'=>'required|image|mimes:jpg,jpeg,png,gif,svg',
            'descripcionAlumno'=>'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }
        else{
            
            $imagen = $request->file('imagen');
            $nombreImagen = time().'.'.$imagen->getClientOriginalExtension();
            $destino = public_path('images/alumnos');
            $request->imagen->move($destino, $nombreImagen);
            $red = Image::make($destino.'/'.$nombreImagen);
            $red->resize(200,null, function($constraint){
                $constraint->aspectRatio();
            });
            $red->save($destino.'/thumbs/'.$nombreImagen);
            
            $alumno = Alumno::create([
                'nombre'=>$request->nombre,
                'descripcion'=>$request->descripcionAlumno,
                'imagen'=>$nombreImagen
            ]);
               

            return back()->with('Listo','Ingresado correctamente');
        
        }
        
    }

    public function getAlumnoDelete($id)
    {
        
        $alumno = Alumno::find($id);
        $imagenPrevia = $alumno->imagen;
        if($alumno->delete()){
            $destino = public_path('images/alumnos');
            unlink($destino.'/'.$imagenPrevia);
            unlink($destino.'/thumbs/'.$imagenPrevia);
            return back()->with('Listo','Registro eliminado exitosamente');
        }
    }
}


