<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SobreNosotros;
use Validator;
use Image;

class SobreNosotrosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $about = \DB::table('sobreNosotros')
                    ->select('sobreNosotros.*')
                    ->orderBy('id','DESC')
                    ->get();
        return view('layouts.admin.sobreNosotros.sobreNosotros')->with('about',$about); 
    }

    public function getSobreNosotrosEdit($id){   
           
        $about = SobreNosotros::find($id);
        return view('layouts.admin.sobreNosotros.editSobreNosotros')->with('about',$about); 

    }

    public function editarSobreNosotros(Request $request)
    {
        
        $sobreNosotros = SobreNosotros::find($request->id);
        $imagenPrevia = $sobreNosotros->imagen;
        $validator = Validator::make($request->all(),[
            'nombre'=>'required|min:3',
            'imagen'=>'image|mimes:jpg,jpeg,png,gif,svg',
            'descripcion'=>'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }else{


            if ($request->hasFile('imagen')){
                $imagen = $request->file('imagen');
                $nombreFoto = time().'.'.$imagen->getClientOriginalExtension();
                $destino = public_path('images/sobreNosotros');
                $request->imagen->move($destino, $nombreFoto);
                $red = Image::make($destino.'/'.$nombreFoto);
                $red->resize(200,null, function($constraint){
                $constraint->aspectRatio();
            });
                $red->save($destino.'/thumbs/'.$nombreFoto);
                unlink($destino.'/'.$imagenPrevia);
                unlink($destino.'/thumbs/'.$imagenPrevia);
                $sobreNosotros->imagen=$nombreFoto; 
    
              }

              $sobreNosotros->nombre= $request->nombre;
              $sobreNosotros->descripcion= $request->descripcion;
              $sobreNosotros->save();
              return redirect('/admin/sobreNosotros')->with('Listo','Registro actualizado exitosamente');
            
           
        }
    }
}
