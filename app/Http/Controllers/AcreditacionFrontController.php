<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AcreditacionFrontController extends Controller
{
    public function index()
    {
        $acreditacion = \DB::table('acreditaciones')
        ->select('acreditaciones.*')
        ->orderBy('id','DESC')
        ->get();

        return view('layouts.acreditacion.acreditacion')->with('acreditacion',$acreditacion); 
    }
}
