<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EscuelaConductoresFrontController extends Controller
{
    public function index()
    {
        $escuelas = \DB::table('escuelas')
        ->select('escuelas.*')
        ->orderBy('id','DESC')
        ->get();

        return view('layouts.escuelaConductores.escuelaConductores')->with('escuelas',$escuelas); 
    }
}
