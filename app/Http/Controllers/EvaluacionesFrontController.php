<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Examen;

class EvaluacionesFrontController extends Controller
{
        /*------------------Funciones para el cliente--------------------- */
        public function index(){

            $evaluacion = \DB::table('evaluaciones')
                        ->select('evaluaciones.*')
                        ->orderBy('id','DESC')
                        ->get();
    
            $examenes = \DB::table('examenes')
                        ->select('examenes.*')
                        ->whereNull('deleted_at')
                        ->orderBy('id','ASC')
                        ->get();
    
    
            return view("layouts.evaluacionesPsicotecnicas", 
                        ['evaluacion' => $evaluacion, 
                        'examenes' => $examenes]);
           
        }

        
    public function edit($id){   
           
        $examen = Examen::find($id);
        return view('layouts.detalleExamen')->with('examen',$examen); 
    
        }
}
