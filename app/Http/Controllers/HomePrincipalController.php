<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomePrincipalController extends Controller
{
  
    public function index()
    {
        $categorias = \DB::table('categoriaCursos')
                    ->select('categoriaCursos.*')
                    ->whereNull('deleted_at')
                    ->orderBy('id','DESC')
                    ->get();

        $about = \DB::table('sobreNosotros')
                    ->select('sobreNosotros.*')
                    ->orderBy('id','DESC')
                    ->get();

        $imagenes = \DB::table('sliders')
                    ->select('sliders.*')
                    ->whereNull('deleted_at')
                    ->orderBy('id','DESC')
                    ->get();

        
        $servicios = \DB::table('servicios')
                    ->select('servicios.*')
                    ->whereNull('deleted_at')
                    ->orderBy('id','DESC')
                    ->get();

        $alumnos = \DB::table('alumnos')
                    ->select('alumnos.*')
                    ->whereNull('deleted_at')
                    ->orderBy('id','DESC')
                    ->get();

    return view("layouts.index", 
                ['categorias' => $categorias, 
                'about' => $about ,
                'imagenes' => $imagenes,
                'servicios' => $servicios,
                'alumnos' => $alumnos]);
    }
}
