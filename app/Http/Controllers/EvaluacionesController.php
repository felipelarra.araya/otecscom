<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Evaluacion;
use App\Examen;
use Validator;
use Image;

class EvaluacionesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index(){

        $evaluacion = \DB::table('evaluaciones')
                    ->select('evaluaciones.*')
                    ->orderBy('id','DESC')
                    ->get();
        return view('layouts.admin.evaluaciones.evaluacionesPsicotecnicas')->with('evaluacion',$evaluacion); 
    }

    public function verExamenes(){

        $examenes = \DB::table('examenes')
                    ->select('examenes.*')
                    ->whereNull('deleted_at')
                    ->orderBy('id','DESC')
                    ->paginate(3);
        return view('layouts.admin.evaluaciones.examenes')->with('examenes',$examenes); 
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'nombre'=>'required|min:3',
            'imagen'=>'required|image|mimes:jpg,jpeg,png,gif,svg',
            'descripcion'=>'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }
        else{
            
            $imagen = $request->file('imagen');
            $nombreImagen = time().'.'.$imagen->getClientOriginalExtension();
            $destino = public_path('images/examenes');
            $request->imagen->move($destino, $nombreImagen);
            $red = Image::make($destino.'/'.$nombreImagen);
            $red->resize(200,null, function($constraint){
                $constraint->aspectRatio();
            });
            $red->save($destino.'/thumbs/'.$nombreImagen);
            
            $examen = Examen::create([
                'nombre'=>$request->nombre,
                'descripcion'=>$request->descripcion,
                'imagen'=>$nombreImagen
            ]);
            return back()->with('Listo','Ingresado correctamente');
        
        }
        
    }

    public function editarExamen(Request $request)
    {
        
        $examen = Examen::find($request->id);
        $imagenPrevia = $examen->imagen;
        $validator = Validator::make($request->all(),[
            'nombre'=>'required|min:3',
            'imagen'=>'image|mimes:jpg,jpeg,png,gif,svg',
            'descripcion'=>'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }else{


            if ($request->hasFile('imagen')){
                $imagen = $request->file('imagen');
                $nombreFoto = time().'.'.$imagen->getClientOriginalExtension();
                $destino = public_path('images/examenes');
                $request->imagen->move($destino, $nombreFoto);
                $red = Image::make($destino.'/'.$nombreFoto);
                $red->resize(200,null, function($constraint){
                $constraint->aspectRatio();
            });
            $red->save($destino.'/thumbs/'.$nombreFoto);
                unlink($destino.'/'.$imagenPrevia);
                unlink($destino.'/thumbs/'.$imagenPrevia);
                $examen->imagen=$nombreFoto; 
    
              }

              $examen->nombre= $request->nombre;
              $examen->descripcion= $request->descripcion;
              $examen->save();
              return redirect('/admin/examenes')->with('Listo','Registro actualizado exitosamente');
           
        }
    }

    public function getExamenesAdd(){

    return view('layouts.admin.evaluaciones.addExamenes'); 
    }

    public function getExamenesEdit($id){   
           
    $examen = Examen::find($id);
   
    return view('layouts.admin.evaluaciones.editExamenes')->with('examen',$examen); 

    }


    public function getExamenDelete($id)
    {
        $examen = Examen::find($id);
        $imagenPrevia = $examen->imagen;
        if($examen->delete()){
            $destino = public_path('images/examenes');
            unlink($destino.'/'.$imagenPrevia);
            unlink($destino.'/thumbs/'.$imagenPrevia);
            return back()->with('Listo','Registro eliminado exitosamente');
        }
    }
    /*--------------------Sección Evaluacion----------------------------------------*/
    public function getEvaluacionesEdit($id){   
           
        $evaluacion = Evaluacion::find($id);
       
        return view('layouts.admin.evaluaciones.editEvaluacionesPsicotecnicas')->with('evaluacion',$evaluacion); 
    
        }

        public function editarSeccionEvaluacion(Request $request)
    {
        
        $evaluacion = Evaluacion::find($request->id);
        $imagenPrevia = $evaluacion->imagen;
        $validator = Validator::make($request->all(),[
            'nombre'=>'required|min:3',
            'imagen'=>'image|mimes:jpg,jpeg,png,gif,svg|max:2048',
            'descripcion'=>'required'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }else{


            if ($request->hasFile('imagen')){
                $imagen = $request->file('imagen');
                $nombreFoto = time().'.'.$imagen->getClientOriginalExtension();
                $destino = public_path('images/evaluaciones');
                $request->imagen->move($destino, $nombreFoto);
                $red = Image::make($destino.'/'.$nombreFoto);
                $red->resize(200,null, function($constraint){
                $constraint->aspectRatio();
            });
                $red->save($destino.'/thumbs/'.$nombreFoto);
                unlink($destino.'/'.$imagenPrevia);
                unlink($destino.'/thumbs/'.$imagenPrevia);
                $evaluacion->imagen=$nombreFoto; 
    
              }

              $evaluacion->nombre= $request->nombre;
              $evaluacion->descripcion= $request->descripcion;
              $evaluacion->save();
              return redirect('/admin/evaluacionesPsicotecnicas')->with('Listo','Registro actualizado exitosamente');
            
           
        }
    }
   
}
