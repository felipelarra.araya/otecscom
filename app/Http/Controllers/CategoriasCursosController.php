<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoriaCurso;
use Validator;
use Image;

class CategoriasCursosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
        $categorias = \DB::table('categoriaCursos')
                    ->select('categoriaCursos.*')
                    ->whereNull('deleted_at')
                    ->orderBy('id','DESC')
                    ->paginate(3);
                    
        return view('layouts.admin.capacitacion.categorias')->with('categorias',$categorias); 
    }

    public function getCategoriasAdd()
    {
        return view('layouts.admin.capacitacion.categoriasAdd');
    }


    public function edit($id){   
           
        $categoria = CategoriaCurso::find($id);
        return view('layouts.admin.capacitacion.categoriaEdit')->with('categoria',$categoria); 
    
        }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'nombre'=>'required|min:3',
            'imagen'=>'required|image|mimes:jpg,jpeg,png,gif,svg'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }
        else{
            
            $imagen = $request->file('imagen');
            $nombreImagen = time().'.'.$imagen->getClientOriginalExtension();
            $destino = public_path('images/categoriasCursos');
            $request->imagen->move($destino, $nombreImagen);
            $red = Image::make($destino.'/'.$nombreImagen);
            $red->resize(200,null, function($constraint){
                $constraint->aspectRatio();
            });
            $red->save($destino.'/thumbs/'.$nombreImagen);
            
            $categoriaCurso = CategoriaCurso::create([
                'nombre'=>$request->nombre,
                'imagen'=>$nombreImagen
            ]);
            return back()->with('Listo','Ingresado correctamente');
        
        }
    }

    public function editarCategoria(Request $request)
    {
        
        $categoria = CategoriaCurso::find($request->id);
        $imagenPrevia = $categoria->imagen;
        $validator = Validator::make($request->all(),[
            'nombre'=>'required|min:3',
            'imagen'=>'image|mimes:jpg,jpeg,png,gif,svg'
        ]);
        if($validator->fails()){
            return back()
            ->withInput()
            ->with('ErrorInsert','Favor llenar los datos')
            ->withErrors($validator);
        }else{


            if ($request->hasFile('imagen')){
                $imagen = $request->file('imagen');
                $nombreFoto = time().'.'.$imagen->getClientOriginalExtension();
                $destino = public_path('images/categoriasCursos');
                $request->imagen->move($destino, $nombreFoto);
                $red = Image::make($destino.'/'.$nombreFoto);
                $red->resize(200,null, function($constraint){
                $constraint->aspectRatio();
            });
            $red->save($destino.'/thumbs/'.$nombreFoto);
                unlink($destino.'/'.$imagenPrevia);
                unlink($destino.'/thumbs/'.$imagenPrevia);
                $categoria->imagen=$nombreFoto; 
    
              }

              $categoria->nombre= $request->nombre;
              $categoria->save();
              return redirect('/admin/categoriaCursos')->with('Listo','Registro actualizado exitosamente');
           
        }
    }

    public function getCategoriaDelete($id)
    {
        $categoria = CategoriaCurso::find($id);
        $imagenPrevia = $categoria->imagen;
        if($categoria->delete()){
            $destino = public_path('images/categoriasCursos');
            unlink($destino.'/'.$imagenPrevia);
            unlink($destino.'/thumbs/'.$imagenPrevia);
            return back()->with('Listo','Registro eliminado exitosamente');
        }
    }
}
