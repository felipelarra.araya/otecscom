<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Capacitacion extends Model
{
    protected $table = "capacitaciones";
    protected $fillable = ['nombre','descripcion','imagen'];
    protected $hidden =   ['created_at','updated_at'];
}
